

class App {

	constructor() {
		
		let newButton = document.getElementById('todo-button');
		if (!newButton) return this._displayErrorConsole('Cannot find button');

		newButton.addEventListener('click', () =>  {
			this._createButtonHandler();
			this._setIdLi();
		});

		let delButton = document.getElementById('todo-delete');
        if (!delButton) return this._displayErrorConsole('Cannot find button');

		delButton.addEventListener('click', () =>  {
			this._deleteLi();
		});

		let editButton = document.getElementById('todo-edit');
        if (!editButton) return this._displayErrorConsole('Cannot find button');

		editButton.addEventListener('click', () =>  {
			this._editLi();
		});

		let saveButton = document.getElementById('todo-save');
        if (!saveButton) return this._displayErrorConsole('Cannot find button');

		saveButton.addEventListener('click', () =>  {
			this._saveLi();
		});

		let errorBlock = document.getElementById('errors');
		errorBlock.addEventListener('click', () =>  {
			errorBlock.innerHTML = '';
		});
	}
	
	_createButtonHandler() {
		let text = this._getFormData();
		if (!text) return false;
		this._createListElement(text);
	}

/* set li params*/ 
	_setIdLi(){  		
		let li = document.getElementsByTagName('li')[0];
		if (!li) return this._displayErrorConsole('Cannot find Any LI element');				
		
		let ident = App.count;
		li.setAttribute('id', ident);
		App.count++;
		this._setEventListener(ident);	
		this._unsetEventListener(ident);	
		
	}

/*  set Listener for LI elem  and  Selected */
	_setEventListener (id) {
		document.getElementById(id).addEventListener("click", () => {
				this._unsetClassWarning();
				document.getElementById(id).className = App.classWarning;
		})
	}
/* unset Select Li */	
	_unsetEventListener (id) {
		document.getElementById(id).addEventListener('dblclick', () => {
				this._unsetClassWarning();
		})
	}


/* get Selected Elements */
	_getSelectedElement(){
		let els = document.getElementsByClassName(App.classWarning);
		if (!els) return this._displayErrorConsole('Cannot find any selected elements');				
		
		return els;
	}

/* get ID Selected Elements */
	_getIdSelectedElement(){
		let els = this._getSelectedElement();
		let ids = [];
		for (var i = 0; i < els.length; i++) {
			ids.push(els[i].id); 
 		}
		return ids[0];
	}

/* set usual class to elements with Class Warning */
	_unsetClassWarning(){
		let els = this._getSelectedElement();
		if (!els) return this._displayErrorConsole('Cannot find selected elements');		
		for (var i = 0; i < els.length; i++) {
			els[i].className = 'list-group-item';
		}
	}

/*edit li*/ 
	_editLi(){
		let el = document.getElementById(this._getIdSelectedElement());	
		if (!el) return this._displayErrorConsole('Cannot find selected element');								
		let textarea = document.getElementById('todo-text');
		if (!textarea) return this._displayErrorConsole('Cannot find text area');				
		textarea.value = '';			  
		textarea.value = el.textContent;		
	}

/* save li */
	_saveLi(){	
		let textarea = document.getElementById('todo-text');
		if (!textarea) return this._displayErrorConsole('Cannot find text area');		
		let el = document.getElementById(this._getIdSelectedElement());	
		if (!el) return this._displayErrorConsole('Cannot find selected element');				
		let newtext = textarea.value;	
		el.innerHTML = '';
		el.innerHTML = newtext;
		textarea.value = '';	

	}
/* delete li*/
	_deleteLi(){
		let els = this._getSelectedElement();
		if (!els) return this._displayErrorConsole('Cannot find selected element');		
		let parent = document.getElementById('todo-list');
		if (!parent) return this._displayErrorConsole('Cannot find List');
		
		let ids = [];
		for (var i = 0; i < els.length; i++) {
			       ids.push(els[i].id); 
		}		
		ids.forEach( function(el, index, ids){
			alert("Do you want to Delete this ITEM?");
			parent.removeChild(document.getElementById(el));
		})
	}   

/* tutor function */
	_getFormData() {
		let textarea = document.getElementById('todo-text');
		if (!textarea) return this._displayErrorConsole('Cannot find text area');

		let text = textarea.value;
		if (text.length > 140 || text.length === 0) return this._displayErrorBlock('Invalid size');

		textarea.value = '';

		return text;
	}

/* tutor function */
	_createListElement(text) {

		return this._displayBlock(text, {
			list: {
				id: 'todo-list',
				errorMessage: 'Cannot find TODO List'
			},
			element: {
				tag: 'li',
				classList: ['list-group-item'],
			}
		});
	}

/* tutor function */
	_displayErrorConsole(text) {
		console.error(text);
		return false;
	}

/* tutor function */
	_displayErrorBlock(text) {
		let list = document.getElementById('errors');
		if (!list) return this._displayErrorConsole('Cannot find errros block.');

		list.innerHtml = '';

		return this._displayBlock(text, {
			list: {
				id: 'errors',
				errorMessage: 'Cannot find errors block'
			},
			element: {
				tag: 'div',
				classList: ['alert', 'alert-danger']
			}
		});
	}

/* tutor function */
	_displayBlock(text, options) {
		let list = document.getElementById(options.list.id);
		if (!list) return this._displayErrorConsole(options.list.errorMessage);

		let element = document.createElement(options.element.tag);
		for (let _class in options.element.classList) {
			element.classList.add(options.element.classList[_class]);
		}
		element.textContent = text;

		list.prepend(element);

		return false;
	}

	
}

(new App());

App.count = 1;  // var for create li ID
App.classWarning = "list-group-item list-group-item-warning";


